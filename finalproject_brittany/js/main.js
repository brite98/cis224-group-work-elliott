$(function() {
    var clickCount = 0;
    var maxCount= 20;
    var matches, $matches;
    var monkeyClasses = ["_SeeNoEvil", "_HearNoEvil", "_SpeakNoEvil", "_SeeNoEvil", "_HearNoEvil", "_SpeakNoEvil"];
    var $monkeys; /* jquery object */
    var pair = [];
    var $resetBtn;

    $monkeys = $("#monkeys div");
    $matches = $('#matches');
    $resetBtn = $("#reset");
    
    if(!localStorage.getItem("matches"))    {
        localStorage.setItem("matches", 0);
    }
    
    function Shuffle(o) {
	   for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	   return o;
    };
    
    function init() {
        Shuffle(monkeyClasses);
        console.log(monkeyClasses);
        clickCount = 0;
        matches = Number(localStorage.getItem("matches"));
        $matches.text(matches);
        pair = [];
        
        for (let i=0; i<$monkeys.length; i++) {
             var $div = $monkeys.get(i);
             $div.setAttribute('class', monkeyClasses[i]);
             console.log($div);
         }   
        
        
    }
    
    function checkForMatch()    {
        let classVal = $(this).attr('class'); 
        
        function updateClass(div)    {
            if (classVal[0] === '_')    {
                classVal = classVal.slice(1);
                div.attr('class', classVal);
                pair.push(classVal);
                clickCount++;
            }
        }
        
        updateClass($(this));
        
        if (clickCount === 2 && pair[0] === pair[1])    {
            matches++;
            $matches.text(matches);
            localStorage.setItem("matches", matches);
            clickCount = 0;
            

        }
        
        if (matches >= maxCount)    {
            matches =0;
            localStorage.setItem("matches", matches);
              
        }
        
        
       /* 
        switch(clickCount)  {
            case 0: 
            case 1:
                updateClass($(this));
                break;
            case 2:
                if (pair[0] === pair[1])  {
                    matches++;
                    $matches.text(matches);
                } 
                break;
            default:
                console.log("uh oh");
                
        }*/
 
    }

    
    function resetGame()    {
        console.log("Resetting game");
        //remove all class attributes
        $monkeys.each(function ()   {
            $(this).removeClass(); 
        });
        
        //re-initialize
        init();
        
    }
    
    $monkeys.on('click', checkForMatch);
    $resetBtn.on('click', resetGame);
    init();
});

